// JavaScript Document

/*
http://dkexit.eu/webdev/site/ch09s04.html

'use strict';

//createPage - Creates page content on load of minimal html

const $ = function (foo) { return document.getElementById(foo);}
 
const createPage = function() {
    let b = $('bod');                               // points to body
    let h1 = document.createElement('h1');
    let h1t = document.createTextNode('Some Page');
    h1.appendChild(h1t);
    h1.setAttribute('style', 'color: yellow');        // style one way
    b.appendChild(h1);
    let j = Math.floor(Math.random() * 9 + 1);
    for (let i = 0; i < 9; i++) {
        let d = document.createElement('div');
        d.style.border = '1px solid blue';          // style another way
        d.style.backgroundColor = 'silver'
        d.setAttribute('class', 'fl');              // ref to stylesheet
        if (i === j) {
            d.style.backgroundColor = 'yellow';
            d.setAttribute('id', 'ttt');
        }
        b.appendChild(d);
    }
    buildTttBoard();
}
window.addEventListener('load', createPage);

*/


function leftFooter() {
    let art = document.getElementById("left");
    let imgImage = document.createElement("img"); //Sådan laver man et img tag element til html. 


    imgImage.setAttribute("src", "img/book-icon.png");
    imgImage.setAttribute("width", "25");
    imgImage.setAttribute("alt", "bog");

    let br = document.createElement('br'); 
    art.appendChild(br); // put on tree

    let paragraph = document.createElement("p");
    let text = document.createTextNode("Bla bla bla");
    paragraph.style.color = "white";
    paragraph.appendChild(text);


    if(art.firstElementChild.matches("img")){
        art.setAttribute('style', 'color: white');
        art.innerHTML = "Click me";
        art.removeChild(imgImage);
        art.removeChild(paragraph);

    }else{
        art.innerHTML = "";
        art.appendChild(imgImage);
        art.appendChild(paragraph);
    }

};

function centerFooter() {
    let art = document.getElementById("center");
    let imgImage = document.createElement("img"); //Sådan laver man et img tag element til html. 


    imgImage.setAttribute("src", "img/book-icon.png");
    imgImage.setAttribute("width", "25");
    imgImage.setAttribute("alt", "bog");

    let br = document.createElement('br'); 
    art.appendChild(br); // put on tree

    let paragraph = document.createElement("p");
    let text = document.createTextNode("Bla bla bla");
    paragraph.style.color = "yellow";
    paragraph.appendChild(text);


    if(art.firstElementChild.matches("img")){
        art.setAttribute('style', 'color: white');
        art.innerHTML = "Click me";
        art.removeChild(imgImage);
        art.removeChild(paragraph);

    }else{
        art.innerHTML = "";
        art.appendChild(imgImage);
        art.appendChild(paragraph);
    }

};



